package Fibonacci;

public class Fibonacci {

	public double calculateNthValue(int n) {
		double[] arrayList = new double[n];
		if (n == 1) {
			arrayList[0] = 1;
		} else {
			arrayList[0] = arrayList[1] = 1;
		}

		for (int i = 0; i < arrayList.length; i++) {
			if (i != 0 && i != 1) {
				arrayList[i] = arrayList[(i - 1)] + arrayList[(i - 2)];
			}
		}
		return arrayList[n - 1];
	}
}
