package Fibonacci;

import java.util.Scanner;


public class FibonacciTest {
	private static Scanner scan;

	public static void main(String[] args) {
		scan = new Scanner(System.in);
		System.out.print("Fibonacci N(N>0)= " );
		int n = scan.nextInt();
		Fibonacci f = new Fibonacci();
		System.out.println("Fibonacci value = "+f.calculateNthValue(n));
	}
}
