package testByJunit;

import static org.junit.Assert.*;
import org.junit.Test;
import Fibonacci.Fibonacci;

public class testFibonacci {
	Fibonacci f = new Fibonacci();

	@Test(timeout = 100) // response time
	public void testCalculateNthValue() throws Exception {

		assertEquals(1.2586269025E10, f.calculateNthValue(50), 0);
		assertEquals(1.0, f.calculateNthValue(1), 0);
		assertEquals(1.0, f.calculateNthValue(2), 0);
		assertEquals(2.0, f.calculateNthValue(3), 0);
		assertEquals(6765, f.calculateNthValue(20), 0);
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void inputtest1() {
		f.calculateNthValue(0);
	}

	@Test(expected = NegativeArraySizeException.class)
	public void inputtest2() {
		f.calculateNthValue(-1);
	}

	@Test(expected = OutOfMemoryError.class)
	public void inputtest3() {
		f.calculateNthValue(2147483647);
	}

}
